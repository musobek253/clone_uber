import 'package:http/http.dart' as http;

class RequestAssistant
{
  static Future<dynamic> getRequest(String url) async{
    http.Response response = await http.get(url);
  }
}